/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexicosintactico;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;


public class Ventana extends javax.swing.JFrame {
 FileNameExtensionFilter filtro= new FileNameExtensionFilter("Archivos txt","word","txt"); //objeto que hará la selección de un txt
    File f;
    JFileChooser j= new JFileChooser();
    String data1 [][]={};
    String cabecera1[]={"No."," Token "," Tipo"};
    String path;
    int cont=0;
    int errores; //variable que almacena la línea de los errores
    //String mensajini="";
    String tipo="";
    public Ventana() {
        initComponents();
        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        txtATexto1 = new javax.swing.JEditorPane();
        Lineas = new javax.swing.JEditorPane();
        LineaError = new javax.swing.JEditorPane();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panel1.setBackground(new java.awt.Color(0, 102, 153));
        panel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabla.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tabla);

        panel1.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(407, 36, 287, 249));

        jLabel2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Inserte Código Aquí");
        jLabel2.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        panel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(55, 11, -1, -1));

        jLabel3.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Análisis Léxico");
        jLabel3.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        panel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(443, 11, -1, -1));

        txtATexto1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        txtATexto1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtATexto1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtATexto1KeyReleased(evt);
            }
        });

        Lineas.setEditable(false);
        Lineas.setText("1");
        Lineas.setOpaque(false);

        LineaError.setEditable(false);
        LineaError.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        LineaError.setForeground(java.awt.Color.red);
        LineaError.setToolTipText("");
        LineaError.setOpaque(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(Lineas, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtATexto1, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LineaError, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(366, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LineaError, javax.swing.GroupLayout.DEFAULT_SIZE, 769, Short.MAX_VALUE)
                    .addComponent(Lineas, javax.swing.GroupLayout.DEFAULT_SIZE, 769, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtATexto1, javax.swing.GroupLayout.PREFERRED_SIZE, 498, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jScrollPane2.setViewportView(jPanel1);

        panel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 36, 361, 249));

        jButton1.setBackground(new java.awt.Color(0, 204, 51));
        jButton1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jButton1.setText("Análizador Léxico");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        panel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 300, 265, 31));

        jButton2.setBackground(new java.awt.Color(0, 204, 51));
        jButton2.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jButton2.setText("Análisis Sintáctico & Semántico");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        panel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 340, 265, 34));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/fondos-de-pantalla-para-programadores-CodeWallpaper3.jpg"))); // NOI18N
        jLabel1.setName(""); // NOI18N
        panel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1050, 390));

        jMenuBar1.setFont(new java.awt.Font("Microsoft JhengHei Light", 2, 12)); // NOI18N

        jMenu1.setText("Opciones");
        jMenu1.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N

        jMenu2.setText("Abrir");
        jMenu2.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N

        jMenuItem1.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/iconos/New document.png"))); // NOI18N
        jMenuItem1.setText("Abrir Archivo.txt");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenu1.add(jMenu2);

        jMenuItem5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jMenuItem5.setText("Instrucciones");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, 703, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        HashMap <String,Integer> r = new HashMap<>();
        HashMap <String,Integer> op = new HashMap<>();
        HashMap <String,Integer> id = new HashMap<>();
        HashMap <String,Integer> deli = new HashMap<>();
        HashMap <String,Integer> num = new HashMap<>();
        LinkedList <String> texto = new LinkedList<>();

        r.put("BEGIN", 0);
        r.put("END", 0);
        r.put("STRING", 0); //String - WORD
        r.put("ALFA", 0);
        r.put("INT", 0); //INT-NUM
        r.put("DOUBLE", 0); //Double -DNUM
        r.put("BOOL", 0);
        r.put("LNUM", 0);
        r.put("TAKE", 0); //pedir datos como un prompt
        r.put("SEND", 0); //mensaje en pantalla como alert
        r.put("WHILE", 0); //Ciclo while - WHEN
        r.put("IF", 0); //condicion if - IT
        r.put("IS", 0);
        r.put("FOR", 0); //ciclo for - START
        r.put("STEP", 0); //ciclo for
        r.put("TO", 0); //ciclo for
        r.put("STOP", 0); //fin de ciclo for
        r.put("SWHEN", 0); //fin de ciclo while
        r.put("COMPLETE", 0); //fin de if

        op.put("/", 0);
        op.put("*", 0);
        op.put("+", 0);
        op.put("-", 0);
        op.put("=", 0);
        op.put("^", 0);
        op.put("<", 0);
        op.put(">", 0);
        op.put("||", 0);
        op.put("&&", 0);

        deli.put("#", 0);
        deli.put(";", 0);
        deli.put("{", 0);
        deli.put("}", 0);
        deli.put(")", 0);
        deli.put(",", 0);
        deli.put("(",0);

        DefaultTableModel model = new DefaultTableModel();
        model.setColumnIdentifiers(new Object[]{"Token","Num","Tipo"});

        StringTokenizer st = new StringTokenizer(txtATexto1.getText(),"{}();,\"=+-*/><||&&# \n\t",true);
        String token, text = "";
        while (st.hasMoreTokens()){
            token = st.nextToken();
            if(!" ".equals(token) && !"\n".equals(token) && !"\t".equals(token)){
                if (r.containsKey(token)) {
                    r.put(token, r.get(token)+1);
                }else {
                    if (op.containsKey(token)) {
                        op.put(token, op.get(token)+1);
                    }else {
                        if (deli.containsKey(token)){
                            deli.put(token, deli.get(token)+1);
                            if("#".equals(token)){
                                token = st.nextToken();
                                while (st.hasMoreTokens() && !"#".equals(token)){
                                    text += token;
                                    token = st.nextToken();
                                }
                                texto.add(text);
                                deli.put(token, deli.get(token)+1);
                                text = "";
                            }
                        }else {
                            if (id.containsKey(token)) {
                                id.put(token, id.get(token)+1);
                            }else {
                                if(token.matches("([0-9]*)|([0-9]*.[0-9]+)")) {
                                    if (num.containsKey(token)) {
                                        num.put(token, num.get(token)+1);
                                    }else num.put(token, 1);
                                }
                                else id.put(token, 1);
                            }
                        }
                    }
                }
            }
        }

        Iterator<String> itr = r.keySet().iterator();
        while(itr.hasNext()){
            token = itr.next();
            if(r.get(token) > 0)model.addRow(new Object[]{token, r.get(token),"Palabra R"});
        }
        itr = op.keySet().iterator();
        while(itr.hasNext()){
            token = itr.next();
            if(op.get(token) > 0) model.addRow(new Object[]{token, op.get(token),"Op"});
        }
        itr = deli.keySet().iterator();
        while(itr.hasNext()){
            token = itr.next();
            if(deli.get(token) > 0) model.addRow(new Object[]{token, deli.get(token),"Delim"});
        }
        itr = id.keySet().iterator();
        while(itr.hasNext()){
            token = itr.next();
            if(id.get(token) > 0) model.addRow(new Object[]{token, id.get(token),"ID"});
        }
        itr = num.keySet().iterator();
        while(itr.hasNext()){
            token = itr.next();
            if(num.get(token) > 0) {
                if(token.matches("[0-9]+"))model.addRow(new Object[]{token, num.get(token),"Número"});
                if(token.matches("[0-9]+.[0-9]+"))model.addRow(new Object[]{token, num.get(token),"Número Decimal"});
            }
        }
        itr = texto.iterator();
        while(itr.hasNext()){
            model.addRow(new Object[]{itr.next(), "1","Texto"});

        }
        tabla.setModel(model);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtATexto1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtATexto1KeyReleased
        StringTokenizer st = new StringTokenizer(txtATexto1.getText(),"\n",true);
        String txt = "",token;
        cont = 1;

        while (st.hasMoreTokens()){
            token= st.nextToken();
            if("\n".equals(token)) cont++;
        }

        for(int i = 1; i <= cont; i++){
            txt += i+"\n";
        }
        Lineas.setText(txt);
    }//GEN-LAST:event_txtATexto1KeyReleased

    private void txtATexto1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtATexto1KeyPressed
        StringTokenizer st = new StringTokenizer(txtATexto1.getText(),"\n",true);
        String txt = "",token;
        LineaError.setText("");
        //Error.setText("");
        cont = 1;

        while (st.hasMoreTokens()){
            token= st.nextToken();
            if("\n".equals(token)) cont++;
        }

        for(int i = 1; i <= cont; i++){
            txt += i+"\n";
        }
        Lineas.setText(txt);
    }//GEN-LAST:event_txtATexto1KeyPressed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        errores=0;
        LinkedList <String> ENT = new LinkedList<>();
        LinkedList <String> DEC = new LinkedList<>();
        LinkedList <String> TEXT = new LinkedList<>();
        LinkedList <String> TAKE = new LinkedList<>();

        String
        simbolo = "([=<>])",
        id = "([(a-z)(A-Z)](\\w)*)",
        num = "((\\d)+)",
        dec = "((\\d)+(\\.)(\\d)+)",
        text = "((((#)[.\\W\\w\\s]*(#))|("+id+"))((\\s)*(\\+)((\\s)*((#)[.\\W\\w\\s]*(#))|("+id+")))*)",
        send = "((\\s)*SEND(\\s)*(\\()(\\s)*((((#)[.\\W\\w\\s]*(#))|("+id+"))((\\s)*(\\+)((\\s)*((#)[.\\W\\w\\s]*(#))|("+id+")))*)(\\s)*(\\))(\\s)*(;))",
        //take = "((\\s)*TAKE(\\b)(\\s)*"+id+"((\\s)*(,(\\s)*"+id+"))*(\\s)*(;))",
        take = "((\\s)*TAKE(\\s)*(\\()(\\s)*((((#)[.\\W\\w\\s]*(#))|("+id+"))((\\s)*(\\+)((\\s)*((#)[.\\W\\w\\s]*(#))|("+id+")))*)(\\s)*(\\))(\\s)*(;))",
        operaciones = "(("+id+"|"+num+"|"+dec+")(\\s)*([+-/*](\\s)*("+id+"|"+num+"|"+dec+"))+)",
        defVal = "((\\s)*"+id+"(\\s)*=(\\s)*("+id+"|"+text+"|"+operaciones+"|"+num+"|"+dec+")(\\s)*(;))",
        defValVar = "((\\s)*"+id+"(\\s)*=(\\s)*("+id+"|"+text+"|"+operaciones+"|"+num+"|"+dec+")(\\s)*)",
        condicion = id+"(\\s)*"+simbolo+"(\\s)*("+id+"|"+num+"|"+dec+")((\\s)*([(&&)(||)](\\s)*"+id+"(\\s)*"+simbolo+"(\\s)*("+id+"|"+num+"|"+dec+")))*",
        var = "((\\s)*((INT)|(DOUBLE)|(STRING))(\\b)(\\s)*("+id+"|"+defValVar+")((\\s)*(,(\\s)*("+id+"|"+defValVar+")))*(\\s)*(;))",
        main = "((\\s)*"+id+"(\\b)(\\s)*BEGIN(\\s)*(\\{)[.\\W\\w\\s]*(END(\\s)*(\\})(\\s)*)$)",
        main2 = "((\\s)*"+id+"(\\b)(\\s)*BEGIN(\\s)*(\\{))",
        main3 = "((\\s)*END(\\s)*(\\})(\\s)*)",

        start2 = "((\\s)*FOR(\\b)(\\s)*("+id+"|"+num+")(\\b)(\\s)*(=)*("+id+"|"+num+")(\\b)(\\s)*(STEP)(\\b)(\\s)*"+num+"(\\s)*[+-]?(\\s)*(\\b)(TO)(\\b)(\\s)*("+id+"|"+num+")(\\s)*(\\{))",
        start3 = "((\\s)*STOP(\\s)*(\\}))",
        when2 = "((\\s)*WHILE(\\s)*(\\()(\\s)*"+condicion+"(\\s)*(\\))(\\s)*(\\{))",
        when3 = "((\\s)*SWHEN(\\s)*(\\}))",
        it2 = "((\\s)*IF(\\s)*(\\()(\\s)*"+condicion+"(\\s)*(\\))(\\s)*(\\{))",
        it3 = "((\\s)*COMPLETE(\\s)*(\\}))",
        entero = "[0-9]*",
        decimal = "[0-9]*.[0-9]+";

        LinkedList <Integer> error = new LinkedList<>();
        StringTokenizer st = new StringTokenizer(txtATexto1.getText(),";{}",true);
        String token = "", txt = "", e;
        int i = 1, mainE = 0, start = 0, when = 0, it = 0, eB = 0;
        //Error.setText("");

        if(txtATexto1.getText().matches(main)) {

            while (st.hasMoreTokens()){
                token = st.nextToken();
                if(st.hasMoreTokens())token = token+st.nextToken();
                if(token.matches("[.\\W\\w\\s]*(\\})") && st.countTokens() == 1){
                    String auxTok = st.nextToken();
                    token = token+(auxTok.substring(0,auxTok.indexOf("\n")));
                }
                StringTokenizer lin = new StringTokenizer(token,"\n",true);
                while (lin.hasMoreTokens()){
                    e = lin.nextToken();
                    if("\n".equals(e)) i++;
                }

                if(token.matches(start2)) start++;
                if(token.matches(start3)) start--;
                if(token.matches(when2)) when++;
                if(token.matches(when3)) when--;
                if(token.matches(it2)) it++;
                if(token.matches(it3)) it--;
                if((st.hasMoreTokens() == false && (start > 0 || when > 0 || it > 0)) || (start < 0 || when < 0 || it < 0)) eB = 1;

                if((token.matches(send) || token.matches(take) || token.matches(var) || token.matches(defVal) || token.matches(main2) || token.matches(main3) || token.matches("(\\s)*(\\$)") || token.matches(start2) || token.matches(start3) || token.matches(when2) || token.matches(when3) || token.matches(it2) || token.matches(it3)) && eB == 0){
                    if(token.matches(take)){

                    }
                    if(token.matches(var)){
                        StringTokenizer stTipo = new StringTokenizer(token," ,;");
                        String tipo = stTipo.nextToken();

                        if(tipo.contains("INT")){

                            while(stTipo.hasMoreTokens()){
                                tipo = stTipo.nextToken();

                                if(ENT.contains(tipo) || DEC.contains(tipo) || TEXT.contains(tipo)|| TAKE.contains(tipo)){
                                    javax.swing.JOptionPane.showMessageDialog(j, "Error variable repetida: "+tipo+" "+i);
                                    //Error.setText("La Variable esta repetida ("+tipo+") "+i+": \n"
                                      //  + "________________________________________________________________________\n"+token);
                                    for(int j = 1; j <i; j++){
                                        txt += "\n";
                                    }
                                    LineaError.setText(txt+" ¡!");
                                    errores=1;
                                    break;
                                }

                                ENT.add(tipo);
                            }
                        }
                        if(tipo.contains("DOUBLE")){

                            while(stTipo.hasMoreTokens()){
                                tipo = stTipo.nextToken();

                                if(ENT.contains(tipo) || DEC.contains(tipo) || TEXT.contains(tipo)|| TAKE.contains(tipo)){
                                    javax.swing.JOptionPane.showMessageDialog(j, "Error variable repetida: "+tipo+" "+i);
                                    
                                    for(int j = 1; j <i; j++){
                                        txt += "\n";
                                    }
                                    LineaError.setText(txt+" ¡!");
                                    errores=1;
                                    break;
                                }

                                DEC.add(tipo);
                            }
                        }
                        if(tipo.contains("TAKE")){

                            while(stTipo.hasMoreTokens()){
                                tipo = stTipo.nextToken();

                                if(ENT.contains(tipo) || DEC.contains(tipo) || TEXT.contains(tipo)|| TAKE.contains(tipo)){
                                    javax.swing.JOptionPane.showMessageDialog(j, "Error variable repetida: "+tipo+" "+i);
                                    //Error.setText("La Variable esta repetida ("+tipo+") "+i+": \n"
                                      //  + "________________________________________________________________________\n"+token);
                                    for(int j = 1; j <i; j++){
                                        txt += "\n";
                                    }
                                    LineaError.setText(txt+" ¡!");
                                    errores=1;
                                    break;
                                }

                                TAKE.add(tipo);
                            }
                        }
                        if(tipo.contains("STRING")){

                            while(stTipo.hasMoreTokens()){
                                tipo = stTipo.nextToken();

                                if(ENT.contains(tipo) || DEC.contains(tipo) || TEXT.contains(tipo)|| TAKE.contains(tipo)){
                                    javax.swing.JOptionPane.showMessageDialog(j, "Error variable repetida: "+tipo+" "+i);
                                    //Error.setText("La variable esta repetida ("+tipo+") "+i+": \n"
                                      //  + "________________________________________________________________________\n"+token);
                                    for(int j = 1; j <i; j++){
                                        txt += "\n";
                                    }
                                    LineaError.setText(txt+" ¡!");
                                    errores=1;
                                    break;
                                }

                                TEXT.add(tipo);
                            }
                        }
                    }
                    if(token.matches(defVal)){
                        StringTokenizer stComprobar = new StringTokenizer(token," \n\t=;");
                        String ID = stComprobar.nextToken(), comprobar = "", tok = "";
                        //System.out.print(ID);
                        while(stComprobar.hasMoreTokens()){
                            comprobar += stComprobar.nextToken();
                        }

                        if(ENT.contains(ID)){
                            StringTokenizer stComprobarE = new StringTokenizer(comprobar,"+*/-");
                            while(stComprobarE.hasMoreTokens()){
                                tok = stComprobarE.nextToken();

                                if(tok.matches(id)){
                                    if(ENT.contains(tok));
                                    else{
                                        javax.swing.JOptionPane.showMessageDialog(j, "Error semántico: "+i+" "+tok);
                                        //Error.setText("ERROR SEMÁNTICO ("+tok+") "+i+": \n"
                                        //    + "________________________________________________________________________\n"+token);
                                        for(int j = 1; j <i; j++){
                                            txt += "\n";
                                        }
                                        LineaError.setText(txt+" ¡!");
                                        errores=1;
                                        break;
                                    }
                                }
                                else{
                                    if(tok.matches(entero));
                                    else{
                                        javax.swing.JOptionPane.showMessageDialog(j, "Error semántico: "+i+" "+tok);
                                        //Error.setText("ERROR SEMÁNTICO ("+tok+") "+i+": \n"
                                        //    + "________________________________________________________________________\n"+token);
                                        for(int j = 1; j <i; j++){
                                            txt += "\n";
                                        }
                                        LineaError.setText(txt+" ¡!");
                                        errores=1;
                                        break;
                                    }
                                }
                            }
                        }
                        else {
                            if(DEC.contains(ID)){
                                StringTokenizer stComprobarD = new StringTokenizer(comprobar,"+*/-");
                                while(stComprobarD.hasMoreTokens()){
                                    tok = stComprobarD.nextToken();

                                    if(tok.matches(id)){
                                        if(DEC.contains(tok));
                                        else{
                                            javax.swing.JOptionPane.showMessageDialog(j, "Error semántico: "+i+" "+tok);
                                            //Error.setText("ERROR SEMÁNTICO ("+tok+") "+i+": \n"
                                            //   + "________________________________________________________________________\n"+token);
                                            for(int j = 1; j <i; j++){
                                                txt += "\n";
                                            }
                                            LineaError.setText(txt+" ¡!");
                                            errores=1;
                                            break;
                                        }
                                    }
                                    else{
                                        if(tok.matches(decimal));
                                        else{
                                            javax.swing.JOptionPane.showMessageDialog(j, "Error semántico: "+i+" "+tok);
                                            javax.swing.JOptionPane.showMessageDialog(j, "Errores: "+1);
                                            //Error.setText("ERROR SEMÁNTICO ("+tok+") "+i+": \n"
                                              //  + "________________________________________________________________________\n"+token);
                                            for(int j = 1; j <i; j++){
                                                txt += "\n";
                                            }
                                            LineaError.setText(txt+" ¡!");
                                            errores=1;
                                            break;
                                        }
                                    }
                                }
                            }
                            else {
                                if(TEXT.contains(ID)){
                                    if(comprobar.matches("((((\")[.\\W\\w\\s]*(\"))|("+id+"))((\\s)*(\\+)((\\s)*((\")[.\\W\\w\\s]*(\"))|("+id+")))*)"));
                                    else {
                                        javax.swing.JOptionPane.showMessageDialog(j, "Error semántico: "+i+" "+token);
                                        //Error.setText("ERROR SEMÁNTICO "+i+": \n"
                                          //  + "________________________________________________________________________\n"+token);
                                        for(int j = 1; j <i; j++){
                                            txt += "\n";
                                        }
                                        LineaError.setText(txt+" ¡!");
                                        errores=1;
                                        break;
                                    }
                                }
                                else{
                                    javax.swing.JOptionPane.showMessageDialog(j, "Variable no declarada: "+i+" "+token);
                                    //Error.setText("Variable no declarada "+i+": \n"
                                        //+ "________________________________________________________________________\n"+token);
                                    for(int j = 1; j <i; j++){
                                        txt += "\n";
                                    }
                                    LineaError.setText(txt+" ¡!");
                                    errores=1;
                                    break;
                                }
                            }
                        }
                    }
                }

                else {
                    if(token.contains("SEND")){
                        //txtATraducido.setText("PRINT");
                        javax.swing.JOptionPane.showMessageDialog(j, "Error al declarar SEND, linea: "+i+" "+token);
                        //Error.setText("Error al declarar sentencia SEND; en la linea "+i+": \n"
                          //  + "\n"+token);
                        errores=1;
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("INT") || token.contains("DOUBLE") || token.contains("STRING")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error al declarar variables, linea: "+i+ " "+token);
                        //Error.setText("Error en declaracion de variables; en la linea "+i+": \n"
                          //  + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("TAKE")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error en TAKE, linea: "+i+" "+token);
                        //Error.setText("Error en lectura de valor TAKE  en la linea "+i+": \n"
                          //  + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("STOP}")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error en ciclo FOR"+i+" "+token);
                        //Error.setText("Cierre de Ciclo START incorrecto  en la linea "+i+": \n"
                            //+ "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("FOR")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error de ciclo for: "+i+ " "+ token);
                        //Error.setText("Inicio de Ciclo START incorrecto  en la linea "+i+": \n"
                          //  + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("SWHEN")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error de ciclo While: "+i+ " "+ token);
                        //Error.setText("Cierre de ciclo WHEN incorrecto en la linea "+i+": \n"
                          //  + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        break;
                    }
                    if(token.contains("WHILE")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error de ciclo while: "+i+ " "+ token);
                        //Error.setText("Inicio de ciclo WHEN incorrecto en la linea "+i+": \n"
                          //  + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("COMPLETE")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error en "+i+ " "+ token);
                        //Error.setText("Cierre de condicion IT incorrecto en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("IF")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error en: "+i+ " "+ token);
                        //Error.setText("Inicio de IT incorrecto; en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    else {
                        javax.swing.JOptionPane.showMessageDialog(j, "Syntax Error xd: "+i+ " "+ token);
                        //Error.setText("Sintaxis Erronea en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                }

            }

        }

        else {
            st = new StringTokenizer(txtATexto1.getText(),";{}",true);
            while (st.hasMoreTokens()){
                token = st.nextToken();
                if(st.hasMoreTokens())token = token+st.nextToken();
                if(token.matches("[.\\W\\w\\s]*(\\})") && st.countTokens() == 1){
                    String auxTok = st.nextToken();
                    token = token+(auxTok.substring(0,auxTok.indexOf("\n")));
                }
                StringTokenizer lin = new StringTokenizer(token,"\n",true);
                while (lin.hasMoreTokens()){
                    e = lin.nextToken();
                    if("\n".equals(e)) i++;
                }
                if(eB == 1) break;
                if(token.matches(start2)) start++;
                if(token.matches(start3)) start--;
                if(token.matches(when2)) when++;
                if(token.matches(when3)) when--;
                if(token.matches(it2)) it++;
                if(token.matches(it3)) it--;
                if((st.hasMoreTokens() == false && (start > 0 || when > 0 || it > 0)) || (start < 0 || when < 0 || it < 0)) eB = 1;

                if((token.matches(send) || token.matches(take) || token.matches(var) || token.matches(defVal) || token.matches(main2) || token.matches(main3) || token.matches("(\\s)*(\\$)") || token.matches(start2) || token.matches(start3) || token.matches(when2) || token.matches(when3) || token.matches(it2) || token.matches(it3)) && eB == 0){
                    //Error.setText("Compilado Exitosamente xD lml");
                    if(token.matches(main3)) eB = 1;
                }

                else {
                    if(token.contains("SEND")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error en:"+i+ " "+ token);
                        //Error.setText("Error al declarar sentencia SEND  en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("INT") || token.contains("DOUBLE") || token.contains("STRING")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error en variable: "+i+ " "+ token);
                        //Error.setText("Error en declaracion de variables  en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("TAKE")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error en: "+i+ " "+ token);
                        //Error.setText("Error en lectura de valor TAKE en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("STOP}")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error de ciclo: "+i+ " "+ token);
                        //Error.setText("Cierre de Ciclo START incorrecto en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("FOR")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error de ciclo: "+i+ " "+ token);
                        //Error.setText("Inicio de Ciclo START incorrecto  en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("SWHEN")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error de cierre: "+i+ " "+ token);
                        //Error.setText("Cierre de ciclo WHEN incorrecto  en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("WHILE")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error de ciclo: "+i+ " "+ token);
                        //Error.setText("Inicio de ciclo WHEN incorrecto  en la linea "+i+": \n"
                          //  + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("COMPLETE")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error de cierre: "+i+ " "+ token);
                        //Error.setText("Cierre de condicion IT incorrecto; en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    if(token.contains("IF")){
                        javax.swing.JOptionPane.showMessageDialog(j, "Error: "+i+ " "+ token);
                        //Error.setText("Inicio de IT incorrecto en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                    else {
                        javax.swing.JOptionPane.showMessageDialog(j, "Syntaxis Error xd"+i+ " "+ token);
                        //Error.setText("Sintaxis Erronea en la linea "+i+": \n"
                        //    + "\n"+token);
                        for(int j = 1; j <i; j++){
                            txt += "\n";
                        }
                        LineaError.setText(txt+" ¡!");
                        errores=1;
                        break;
                    }
                }
            }
            if(mainE == 0) {
                javax.swing.JOptionPane.showMessageDialog(j, "Error en la línea: "+i+" "+token);
                //Error.setText("Cierre de Clase incorrecto en la Linea "+i+": \n"
                    //+ "\n"+token);
                for(int j = 1; j <1; j++){
                    txt += "\n";
                }
                LineaError.setText(txt+" ¡!");
                errores=1;
            }
        }
        if(errores==1){
            javax.swing.JOptionPane.showMessageDialog(j, "Se han encontrado errores sintácticos/semánticos");
            //btnTraducir.setEnabled(false);
        }else{
            //btnTraducir.setEnabled(true);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        Instrctivo in= new Instrctivo();
        in.show();

    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        //Se crea un jfilechooser
        j.setCurrentDirectory(new File("src\\lexicosintactico"));
        j.getSelectedFile(); //obtiene el archivo
        j.setFileFilter(filtro);//Añado el filtro
        j.showOpenDialog(j);

        int contPalabra=0;//Creo un contador para las palabras
        try{
            //Aqui se manda la ruta del archivo
            path= j.getSelectedFile().getAbsolutePath();//Obtiene la Ruta
            String name=j.getSelectedFile().getName();//Obtiene el nombre
            String lectura="";
            f = new File(path);

            try{

                FileReader fr = new FileReader(f);
                BufferedReader br = new BufferedReader(fr);
                String aux;
                //Aqui cuento cuantas palabras hay
                StreamTokenizer st=new StreamTokenizer(new FileReader(f));
                while(st.nextToken()!=StreamTokenizer.TT_EOF){
                    if(st.ttype==StreamTokenizer.TT_WORD){
                        contPalabra++;

                    }
                    //lblPalabras.setText("Total de Palabras:"+contPalabra);
                    //txtNombre.setText(name);
                    //txtRuta.setText(path);

                }

                //Aqui empieza a leer el archivo linea por linea hasta que en el texto ya no haya nada

                while((aux = br.readLine())!=null)

                lectura = lectura+aux+"\n";//Voy acumulando todo en un string

            }catch(IOException e){}

            txtATexto1.setText(lectura);//Mando lo que resulto de la lectura
            int contador=0;
            StringTokenizer st = new StringTokenizer(txtATexto1.getText(),"\n",true);
            String Text = "",token;
            contador = 1;

            while (st.hasMoreTokens()){
                token= st.nextToken();
                if("\n".equals(token)) contador++;
            }

            for(int i = 1; i <= contador; i++){
                Text += i+"\n";
            }
            Lineas.setText(Text);

            //contarCaracteres(lectura);//Mando llamar el metodo de contar caracteres
            //mayusculasyminusculas(lectura);
        }catch(NullPointerException e){

            javax.swing.JOptionPane.showMessageDialog(j, "Has seleccionado cerrar programa, saliendo...");

            System.exit(0);

        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }
   
        public void Guardar()
    {
        try
        {
            j = new JFileChooser();
           
            
            j.setFileSelectionMode( JFileChooser.FILES_ONLY );
            FileNameExtensionFilter filtroTxt=new FileNameExtensionFilter("Documento de Texto","txt");
            j.setFileFilter(filtroTxt);
            j.setFileHidingEnabled(false);
            int fin = this.getTitle().lastIndexOf('.');
            if(fin == -1)fin = this.getTitle().length();
            j.setSelectedFile(new File(this.getTitle().substring(0,fin)));
          
            int select = j.showSaveDialog(this);
            File guarda = j.getSelectedFile();
            
            if(select == JFileChooser.APPROVE_OPTION)
            {
                if(guarda !=null)
                {
                    FileWriter  save=new FileWriter(guarda+".txt");
                    save.write(txtATexto1.getText());
                    save.close();
                    JOptionPane.showMessageDialog(null,"Se ha guardado el archivo","Información",JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
        catch(IOException ex)
        {
            JOptionPane.showMessageDialog(null,"Su archivo no se ha guardado","Advertencia",JOptionPane.WARNING_MESSAGE);
        } 
    }         
        public void Guardarbas()
    {
        try
        {
            j = new JFileChooser();
           
            
            j.setFileSelectionMode( JFileChooser.FILES_ONLY );
            FileNameExtensionFilter filtroTxt=new FileNameExtensionFilter("Archivos BAS","bas");
            j.setFileFilter(filtroTxt);
            j.setFileHidingEnabled(false);
            int fin = this.getTitle().lastIndexOf('.');
            if(fin == -1)fin = this.getTitle().length();
            j.setSelectedFile(new File(this.getTitle().substring(0,fin)));
          
            int select = j.showSaveDialog(this);
            File guarda = j.getSelectedFile();
            
            if(select == JFileChooser.APPROVE_OPTION)
            {
                if(guarda !=null)
                {
                    FileWriter  save=new FileWriter(guarda+".bas");
                    //save.write(txtATraducido.getText());
                    save.close();
                    JOptionPane.showMessageDialog(null,"Se ha guardado el archivo","Información",JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
        catch(IOException ex)
        {
            JOptionPane.showMessageDialog(null,"Su archivo no se ha guardado","Advertencia",JOptionPane.WARNING_MESSAGE);
        } 
    }         
 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JEditorPane LineaError;
    private javax.swing.JEditorPane Lineas;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel panel1;
    private javax.swing.JTable tabla;
    private javax.swing.JEditorPane txtATexto1;
    // End of variables declaration//GEN-END:variables
}
